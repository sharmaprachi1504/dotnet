FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY D:/TeamCity/buildAgent/work/a6654114fd4b771c/bin/Debug/DotnetHelloworld.exe .
COPY D:/TeamCity/buildAgent/work/a6654114fd4b771c/* .
ENTRYPOINT ["DotnetHelloworld.exe"]
